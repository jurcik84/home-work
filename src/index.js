
import React from 'react'
import ReactDOM from 'react-dom';


// Views
import HeaderView from './view-components/header'
import MainBodyView from './view-components/main-body'
import FooterView from './view-components/footer'

// SASS > CSS
import './index.scss';




const root = document.querySelector("#root")

const Index =()=> <div>
       <HeaderView />
       <MainBodyView />
       <FooterView />
</div>

ReactDOM.render(<Index />, root)